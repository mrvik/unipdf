# UniPDF - PDF for Go

This package is a fork of unidoc/unipdf.

You may wonder why we're not upgrading version to >3.9. After this version, unidoc guys decided to remove AGPL license and leave only the commercial one.
This lib remains AGPL3. We'll try to maintain this. Also this lib assumes by default you're using a Community license.
